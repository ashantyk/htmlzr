(function ( $ ) {
 
    $.fn.htmlzr = function( definition , userSettings ) {

        var settings = $.extend({
            events: true,
            append: false
        }, userSettings );

        // generator function 
        var parseElement = function (definitionFragment, appendTo){

            // detect type and create element
            if(!definitionFragment.tag){
                throw {
                    name        : "Parse Error",
                    level       : "Syntax",
                    message     : "Object is missing 'tag' property",
                    htmlMessage : "Object is missing 'tag' property",
                    toString    : function(){ return this.name + ": " + this.message + ". JSON: " + JSON.stringify(definitionFragment); }
                };
            } 

            var htmlFragment = $('<' + definitionFragment.tag + '>');
            delete definitionFragment.tag;
            appendTo.append(htmlFragment);

            // handle simple content (html)
            if(definitionFragment.html){
                htmlFragment.html(definitionFragment.html);
                delete definitionFragment.html;
            }

            // handle simple content (text)
            if(definitionFragment.text){
                htmlFragment.text(definitionFragment.text);
                delete definitionFragment.text;
            }

            // handle children
            if(definitionFragment.items){
                if($.type(definitionFragment.items) != 'array'){
                    definitionFragment.items = [definitionFragment.items];
                }
                for(var i in definitionFragment.items){
                    parseElement(definitionFragment.items[i], htmlFragment);
                }
                delete definitionFragment.items;
            }

            // bind events
            if(definitionFragment.events){
                if(settings.events){
                    for(var eventName in definitionFragment.events){
                        var handler = definitionFragment.events[eventName];
                        htmlFragment.on(eventName, handler);
                    }
                }
                delete definitionFragment.events;
            }

            // classes
            if(definitionFragment.cls){
                switch($.type(definitionFragment.cls)){
                    case 'string':
                        htmlFragment.addClass(definitionFragment.cls);
                        break;
                    case 'array':
                        htmlFragment.addClass(definitionFragment.cls.join(" "));
                        break;
                }
                delete definitionFragment.cls;
            }

            // css (style)
            if(definitionFragment.css){
                switch($.type(definitionFragment.css)){
                    case 'string':
                        htmlFragment.attr("style", definitionFragment.css);
                        break;
                    case 'object':
                        htmlFragment.css(definitionFragment.css);
                        break;
                }
                delete definitionFragment.css;
            }

            // other attributes
            for (var property in definitionFragment) {
                var value = definitionFragment[property];
                htmlFragment.attr(property, value);
            }

        };

        if(!settings.append){
            // clear 
            this.html('');
        }

        // clone definition
        var clonedDefinition = $.extend(true, $.isArray(definition) ? [] : {}, definition);

        // make sure definition is array
        if(!$.isArray(clonedDefinition)){
            clonedDefinition = [clonedDefinition];
        }

        // generate HTML
        for(var i=0; i < clonedDefinition.length; i++){
            parseElement(clonedDefinition[i], this);    
        }

        // chaining
        return this;
 
    };
 
}( jQuery ));