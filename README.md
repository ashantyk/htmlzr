# htmlzr #

htmlzr is a jQuery plugin used to convert JS objects to HTML elements.

### Basic example ###

```javascript
$.htmlzr({
	tag: 'div',
	text: 'This is an example div block.'
});
```
would create the following

```html
<div>
	This is an example div block.
</div>
```


To append the generated HTML code to an element in the web page do it like so:
```javascript
$('anyTypeOfSelector').htmlzr( jsDefinition );
```


For more examples see the .html files in the /examples directory.


### Property types ###

Property | Expected value type | Description | Example value
---------|---------------------|-------------|--------------
**tag** | string | The type of HTML element to create. Usualy 'div'. | 'div', 'span', 'p' and so on
**cls** | string, array | A string or array of strings, containing the classes for the element. | 'class1 class2' **or** ['class1', 'class2']
**css** | string, object | An object containing all the **inline** CSS definitions for this element. Or you can simply write the CSS yourself in a string. | { margin: '10px', 'border-left': '5px solid black' } **or** "margin:12px; border-left: 5px solid black;"
**events** | object | An object containing eventName - eventHandlers pairs. | { click: function(){ /* do smth. on click event */ } }
**html** | string | Anything this property contains will be added to the created element's body using $.html(). | '<a href="#">Some link</a> to some page.'
**text** | string | Add some text to the created element's body using $.text(). | "Some text to put inside a paragraph."
**items** | array | An array of objects containing child elements to render inside the current element's body. The childs in this array obey the same rules (this table). | [ { type: 'p', text: 'Lorem ipsum' }, { type:'div', html: '<a href="#">Some link</a>' } ] 

Other properties than the ones in the table above are treated as attributes of the HTML element that is created by htmlzr, so if your definition is something like this:
```javascript
{
    tag: 'div',
    cls: 'someClass',
    id: 'div1',
    someAttr: 'someAttrValue',
    text: 'some text in a div block'
}
```
the resulting HTML will be:
```html
<div class="someClass" id="div1" someAttr="someAttrValue">
    some text in a div block
</div>
```


### License ###

This plugin is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This plugin is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

See [http://www.gnu.org/licenses/](http://www.gnu.org/licenses/) for more details.